A slightly modified vision_opencv package for interfacing ROS with newer versions of OpenCV (4.3.0 tested)

Works with Ubuntu 16.04 and ROS Kinetic.
Works with Ubuntu 18.04 and ROS Melodic.
